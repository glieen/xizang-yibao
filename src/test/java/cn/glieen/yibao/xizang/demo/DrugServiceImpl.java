package cn.glieen.yibao.xizang.demo;


import cn.glieen.yibao.xizang.api.impl.API1301;
import cn.glieen.yibao.xizang.pojo.FileBean;
import cn.glieen.yibao.xizang.util.FileUtil;
import cn.glieen.yibao.xizang.util.YibaoUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/6 JDK8
 * <p>
 * 药品目录
 */
@Slf4j
@Service
public class DrugServiceImpl {
    public List<Drug> getAllDrug() {
        List<Drug> list = new ArrayList<>();
        FileBean.Param param = new FileBean.Param();
        API1301 api = new API1301(param);
        param.setVer(API1301.initVer);
        while (true) {
            try {
                List<String[]> fileLines = YibaoUtil.getFileAsList(api, "1");
                List<Drug> temp = FileUtil.parse(fileLines, API1301Handler::parse);
                log.info("当前版本{}加载{}条记录", param.getVer(), temp.size());
                list.addAll(temp);
                Drug max = temp.stream()
                        .max((a, b) -> String.CASE_INSENSITIVE_ORDER.compare(a.getVerName(), b.getVerName()))
                        .orElseThrow(() -> new RuntimeException("未获取到最大版本号"));
                param.setVer(max.getVerName());
                log.info("下一次的最大版本号是:{}", max.getVerName());
            } catch (RuntimeException e) {
                break;
            }
        }
        log.info("总共加载{}条记录", list.size());
        return list;
    }
}
