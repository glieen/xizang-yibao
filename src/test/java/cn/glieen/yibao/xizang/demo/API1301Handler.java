package cn.glieen.yibao.xizang.demo;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/13 JDK8
 */
public class API1301Handler {
    public static Drug parse(String[] line) {
        Drug drug = new Drug();
        drug.setDrugCode(line[1 - 1]);
        drug.setProdName(line[2 - 1]);
        drug.setDrugName(line[4 - 1]);
        drug.setVerNo(line[83 - 1]);
        drug.setVerName(line[84 - 1]);
        return drug;
    }
}
