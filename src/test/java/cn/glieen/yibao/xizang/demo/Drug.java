package cn.glieen.yibao.xizang.demo;

import lombok.Data;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/13 JDK8
 */
@Data
public class Drug {
    /**
     * 医疗目录编码
     */
    private String drugCode;
    /**
     * 商品名称
     */
    private String prodName;
    /**
     * 通用名名称
     */
    private String drugName;
    /**
     * 版本号
     */
    private String verNo;
    /**
     * 版本名称
     */
    private String verName;
}
