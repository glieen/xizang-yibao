package cn.glieen.yibao.xizang.test;

import cn.glieen.yibao.xizang.api.impl.API1317;
import cn.glieen.yibao.xizang.api.impl.API9101;
import cn.glieen.yibao.xizang.pojo.OpterType;
import cn.glieen.yibao.xizang.pojo.PageBean;
import cn.glieen.yibao.xizang.pojo.YibaoProperties;
import cn.glieen.yibao.xizang.util.SignUtil;
import cn.glieen.yibao.xizang.util.YibaoUtil;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Glieen glieen1995@gmailS.com
 * @version 1.0
 * @since 2021/7/13 JDK8
 */
public class SimpleTest {

    static {
        YibaoProperties properties = new YibaoProperties();
        properties.setUrl("http://192.168.1.15:20002/power-csb/powercsb/fsi/api");
        properties.setApiVersion("1.0.0");
        properties.setApiAccessKey("hlsEkjNST7fLpgmUawWreAQkTXbdsYv6dJFSYq");
        properties.setSecretKey("SK98spEaxeWc1RZXOHQLwKJ4wlV4zgP0A4JoJknj");
        properties.setConnectionTimeout(30);
        properties.setSocketTimeout(30);
        properties.setInfver("V1.0");
        properties.setOrgCode("H54030200001");
        properties.setOrgName("昌都市藏医院");
        properties.setSysCode("HIS");
        properties.setAreaAdmvs("540000");
        SignUtil.signIn("1", "Glieen", OpterType.USER);
    }

    @Test
    public void test() {
        API1317.Param param = new API1317.Param();
        param.setUpdt_time(LocalDate.now().minusDays(1));
        param.setFixmedins_code(YibaoProperties.orgCode);
        param.setPage_num(1);
        param.setPage_num(1);
        param.setPage_size(5000);

        API1317 api = new API1317(param);

        PageBean<API1317.Record> page = YibaoUtil.getPage(api, "1");
        System.out.println(page);
    }

    @Test
    public void upload() {
        List<String[]> lines = new ArrayList<>();
        lines.add(new String[]{"1", "2", "3"});
        lines.add(new String[]{"1", "2", "3"});
        lines.add(new String[]{"1", "2", "3"});
        lines.add(new String[]{"1", "2", "3"});
        byte[] bytes = YibaoUtil.string2ZipByte(lines);

        API9101.Param param = new API9101.Param();
        param.setFilename("test.zip");
        param.setFixmedins_code(YibaoProperties.orgCode);
        param.setIn(bytes);
        API9101 api = new API9101(param);

        API9101.Output result = YibaoUtil.getResult(api, "1");
        System.out.println(result);
    }
}
