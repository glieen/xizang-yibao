package cn.glieen.yibao.xizang.test;

import cn.glieen.yibao.xizang.YibaoApplication;
import cn.glieen.yibao.xizang.demo.DrugServiceImpl;
import cn.glieen.yibao.xizang.pojo.OpterType;
import cn.glieen.yibao.xizang.util.SignUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/13 JDK8
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = YibaoApplication.class)
public class ApplicationTest {
    @Autowired
    private DrugServiceImpl drugService;

    @Test
    public void test() {
        SignUtil.signIn("1", "Glieen", OpterType.USER);
        drugService.getAllDrug();
    }
}
