package cn.glieen.yibao.xizang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/13 JDK8
 */
@SpringBootApplication
public class YibaoApplication {
    public static void main(String[] args) {
        SpringApplication.run(YibaoApplication.class, args);
    }
}
