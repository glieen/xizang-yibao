package cn.glieen.yibao.xizang.pojo;

import lombok.Data;

import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/14 JDK8
 */
@Data
public class PageBean<T> {
    /**
     * 记录总数
     */
    private Integer recordCounts;
    /**
     * 总页数
     */
    private Integer pages;
    /**
     * 分页内容
     */
    private List<T> data;
    /**
     * 是否首页
     */
    private Boolean firstPage;
    /**
     * 是否末页
     */
    private Boolean lastPage;
    /**
     * 数据量
     */
    private Integer size;
    /**
     * 开始行数
     */
    private Integer startRow;
    /**
     * 结束行数
     */
    private Integer endRow;
    /**
     * 分页大小
     */
    private Integer pageSize;
    /**
     * 当前页数
     */
    private Integer pageNum;
}
