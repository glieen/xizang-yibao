package cn.glieen.yibao.xizang.pojo;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/6 JDK8
 * <p>
 * 请求对象
 */
@Data
public class RequestObject {
    /**
     * 交易编号
     */
    private String infno;
    /**
     * 发送方报文 ID
     */
    private String msgid;
    /**
     * 就医地医保区划
     */
    private String mdtrtarea_admvs;
    /**
     * 参保地医保区划
     */
    private String insuplc_admdvs;
    /**
     * 接收方系统代码
     */
    private String recer_sys_code;
    /**
     * 设备编号
     */
    private String dev_no;
    /**
     * 设备安全信息
     */
    private String dev_safe_info;
    /**
     * 数字签名信息
     */
    private String cainfo;
    /**
     * 签名类型
     */
    private String signtype;
    /**
     * 接口版本号
     */
    private String infver;
    /**
     * 经办人类别
     */
    private String opter_type;
    /**
     * 经办人
     */
    private String opter;
    /**
     * 经办人姓名
     */
    private String opter_name;
    /**
     * 交易时间
     */
    private LocalDateTime inf_time;
    /**
     * 定点医药机构编号
     */
    private String fixmedins_code;
    /**
     * 定点医药机构名称
     */
    private String fixmedins_name;
    /**
     * 交易签到流水号
     */
    private String sign_no;
    /**
     * 交易输入
     */
    private Map<String, Object> input;
}
