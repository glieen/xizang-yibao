package cn.glieen.yibao.xizang.pojo;

import lombok.Data;

import java.util.Map;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/6 JDK8
 * <p>
 * 响应对象
 */
@Data
public class ResponseObject {
    /**
     * 交易状态码
     */
    private Integer infcode;
    /**
     * 接收方报文 ID
     */
    private String inf_refmsgid;
    /**
     * 接收报文时间
     */
    private String refmsg_time;
    /**
     * 响应报文时间
     */
    private String respond_time;
    /**
     * 错误信息
     */
    private String err_msg;
    /**
     * 交易输出
     */
    private Map<String, Object> output;

    /**
     * 是否成功
     */
    public boolean success() {
        return this.infcode == 0;
    }
}
