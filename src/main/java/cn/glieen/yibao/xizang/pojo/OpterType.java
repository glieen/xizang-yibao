package cn.glieen.yibao.xizang.pojo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/7 JDK8
 * <p>
 * 经办人类型
 */
@Getter
@ToString
@AllArgsConstructor
public enum OpterType {
    USER("1", "经办人"),
    TERMINAL("2", "自助终端"),
    MOBILE("3", "移动终端"),
    ;

    private final String code;
    private final String name;
}
