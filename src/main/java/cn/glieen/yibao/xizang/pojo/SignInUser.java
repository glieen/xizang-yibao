package cn.glieen.yibao.xizang.pojo;

import lombok.Data;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/7 JDK8
 */
@Data
public class SignInUser {
    /**
     * 操作员编号
     */
    private String opterNo;
    /**
     * 操作人名称
     */
    private String opterName;
    /**
     * 操作人类型
     */
    private OpterType opterType;
    /**
     * Mac地址
     */
    private String mac;
    /**
     * IP
     */
    private String ip;
    /**
     * 签到编号
     */
    private String signNo;
}
