package cn.glieen.yibao.xizang.pojo;

import cn.glieen.yibao.xizang.api.ApiParam;
import lombok.Data;

import java.time.LocalDate;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/7 JDK8
 */
public class FileBean {
    @Data
    public static class Param implements ApiParam {
        /**
         * 版本号
         */
        private String ver;

        @Override
        public String paramName() {
            return "data";
        }
    }

    @Data
    public static class Output {
        /**
         * 文件查询号
         */
        private String file_qury_no;
        /**
         * 文件名
         */
        private String filename;
        /**
         * 下载截止日期
         */
        private LocalDate dld_end_time;
        /**
         * 数据量
         */
        private Long data_cnt;
    }
}