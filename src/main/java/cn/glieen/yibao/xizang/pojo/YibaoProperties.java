package cn.glieen.yibao.xizang.pojo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/6 JDK8
 * <p>
 * 请求头参数
 */
@Configuration
public class YibaoProperties {
    /**
     * 医保中心API地址
     */
    public static String url;
    /**
     * 连接超时
     */
    public static int connectionTimeout;
    /**
     * 响应超时
     */
    public static int socketTimeout;
    /**
     * 服务网关中的服务版本号
     */
    public static String apiVersion;
    /**
     * 服务网关中的AK(accessKey)
     */
    public static String apiAccessKey;
    /**
     * 服务网关中的SK(secretKey)
     */
    public static String secretKey;
    /**
     * 医疗机构编码
     */
    public static String orgCode;
    /**
     * 医疗机构名称
     */
    public static String orgName;
    /**
     * 就医地区划代码
     */
    public static String areaAdmvs;
    /**
     * 接口版本号
     */
    public static String infver;
    /**
     * 系统代码
     */
    public static String sysCode;

    @Value("${yibao.config.xizang.url}")
    public void setUrl(String url) {
        YibaoProperties.url = url;
    }

    @Value("${yibao.config.xizang.apiVersion}")
    public void setApiVersion(String apiVersion) {
        YibaoProperties.apiVersion = apiVersion;
    }

    @Value("${yibao.config.xizang.apiAccessKey}")
    public void setApiAccessKey(String apiAccessKey) {
        YibaoProperties.apiAccessKey = apiAccessKey;
    }

    @Value("${yibao.config.xizang.secretKey}")
    public void setSecretKey(String secretKey) {
        YibaoProperties.secretKey = secretKey;
    }

    @Value("${yibao.config.xizang.connectionTimeout}")
    public void setConnectionTimeout(int connectionTimeout) {
        YibaoProperties.connectionTimeout = connectionTimeout;
    }

    @Value("${yibao.config.xizang.socketTimeout}")
    public void setSocketTimeout(int socketTimeout) {
        YibaoProperties.socketTimeout = socketTimeout;
    }

    @Value("${yibao.config.xizang.orgCode}")
    public void setOrgCode(String orgCode) {
        YibaoProperties.orgCode = orgCode;
    }

    @Value("${yibao.config.xizang.orgName}")
    public void setOrgName(String orgName) {
        YibaoProperties.orgName = orgName;
    }

    @Value("${yibao.config.xizang.areaAdmvs}")
    public void setAreaAdmvs(String areaAdmvs) {
        YibaoProperties.areaAdmvs = areaAdmvs;
    }

    @Value("${yibao.config.xizang.infver}")
    public void setInfver(String infver) {
        YibaoProperties.infver = infver;
    }

    @Value("${yibao.config.xizang.sysCode}")
    public void setSysCode(String sysCode) {
        YibaoProperties.sysCode = sysCode;
    }
}
