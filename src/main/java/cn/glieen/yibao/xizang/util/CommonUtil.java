package cn.glieen.yibao.xizang.util;

import org.springframework.util.StringUtils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/8 JDK8
 */
public class CommonUtil {

    public static List<String[]> splitStringAsList(String s) {
        if (StringUtils.isEmpty(s)) {
            return Collections.emptyList();
        }
        String[] lines = s.split("\n");
        return Arrays.stream(lines).filter(line -> line.length() > 1).map(
                line -> {
                    String[] ret = line.split("\t");
                    for (int i = 0; i < ret.length; i++) {
                        if ("null".equals(ret[i])) {
                            ret[i] = null;
                        }
                    }
                    return ret;
                }
        ).collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    public static <T> Class<T> getGenericType(Class<?> clazz, Class<?> genericClass) {
        Type[] types = Objects.requireNonNull(clazz).getGenericInterfaces();
        for (Type type : types) {
            if (!type.getTypeName().contains(Objects.requireNonNull(genericClass).getSimpleName())) {
                continue;
            }
            return (Class<T>) ((ParameterizedType) type).getActualTypeArguments()[0];
        }
        throw YibaoException.exception("未实现泛型结果接口" + genericClass.getSimpleName());
    }
}
