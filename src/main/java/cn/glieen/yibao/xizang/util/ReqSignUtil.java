package cn.glieen.yibao.xizang.util;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/6 JDK8
 * <p>
 * 请求签名工具
 */
public class ReqSignUtil {

    /**
     * 生成签名
     *
     * @param secretKey 私钥
     * @param params    参数
     * @return 签名
     */
    public static String generateSign(final String secretKey, final Map<String, String> params) {
        // 对参数名进行排序
        List<String> storedKeys = Arrays.stream(params.keySet()
                        .toArray(new String[]{}))
                .sorted(Comparator.naturalOrder())
                .collect(Collectors.toList());
        // 根据排序后的参数名集合对参数进行拼接
        final String data = storedKeys.stream()
                .map(key -> String.join("=", key, params.get(key)))
                .collect(Collectors.joining("&")).trim();
        // 生成签名的base64字符串
        return generateSignBase64(secretKey, data);
    }

    /**
     * 生成签名数组
     *
     * @param secretKey 密钥
     * @param data      待签名数据
     * @return String 签名
     */
    public static byte[] generateSign(final String secretKey, final String data) {
        Mac mac;
        SecretKeySpec secret;
        byte[] hash;
        try {
            mac = Mac.getInstance("HmacSHA1");
            secret = new SecretKeySpec(secretKey.getBytes(StandardCharsets.UTF_8), mac.getAlgorithm());
            mac.init(secret);
            hash = mac.doFinal(data.getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            throw YibaoException.exception("生成凭证失败:" + e.getMessage());
        }
        return hash;
    }

    /**
     * 生成签名base64字符串
     *
     * @param secretKey 私钥
     * @param data      签名数据
     * @return 签名
     */
    public static String generateSignBase64(final String secretKey, final String data) {
        return new String(Base64.getEncoder().encode(generateSign(secretKey, data)), StandardCharsets.UTF_8);
    }
}

