package cn.glieen.yibao.xizang.util;


import cn.glieen.yibao.xizang.api.impl.API9001;
import cn.glieen.yibao.xizang.api.impl.API9002;
import cn.glieen.yibao.xizang.pojo.OpterType;
import cn.glieen.yibao.xizang.pojo.RequestObject;
import cn.glieen.yibao.xizang.pojo.SignInUser;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/7 JDK8
 */
@Slf4j
public class SignUtil {
    private static final Map<String, SignInUser> session = new ConcurrentHashMap<>();
    private static final Random random = new Random();

    public static String signIn(String opter_no, String opter_name, OpterType opterType) {
        return signIn(opter_no, opter_name, opterType, randomIP(), randomMac());
    }

    public static String signIn(String opter_no, String opter_name, OpterType opterType, String ip, String mac) {
        log.info("医保签到");
        SignInUser user = session.get(opter_no);
        if (user != null) {
            return user.getSignNo();
        }
        user = new SignInUser();
        user.setIp(ip);
        user.setMac(mac);
        user.setOpterName(opter_name);
        user.setOpterNo(opter_no);
        user.setOpterType(opterType);

        API9001.Param param = new API9001.Param();
        param.setIp(user.getIp());
        param.setMac(user.getMac());
        param.setOpter_no(user.getOpterNo());
        API9001 api = new API9001(param);

        RequestObject request = YibaoUtil.createRequest(api, opter_no, true);
        request.setOpter(user.getOpterNo());
        request.setOpter_name(user.getOpterName());
        request.setOpter_type(opterType.getCode());
        API9001.Output result = YibaoUtil.getResult(api, request);
        log.info("医保签到成功:{}", result);

        user.setSignNo(result.getSigninoutb().getSign_no());
        session.put(user.getOpterNo(), user);
        return user.getSignNo();
    }

    public static boolean signOut(String opter_no) {
        log.info("医保签退");
        SignInUser user = session.get(opter_no);
        if (user == null) {
            return true;
        }

        API9002.Param signOut = new API9002.Param();
        signOut.setSign_no(user.getSignNo());
        signOut.setOpter_no(user.getOpterNo());
        API9002 api = new API9002(signOut);

        API9002.Output result = YibaoUtil.getResult(api, opter_no);
        log.info("医保签退成功{}", result);

        session.remove(opter_no);
        return true;
    }

    public static SignInUser getSession(String opter_no) {
        return session.get(opter_no);
    }

    private static String randomIP() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            sb.append(random.nextInt(254) + 1);
            if (i != 3) {
                sb.append('.');
            }
        }
        return sb.toString();
    }

    private static String randomMac() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            sb.append(String.format("%02x", random.nextInt(0xff)));
            if (i != 5) {
                sb.append(':');
            }
        }
        return sb.toString();
    }

}
