package cn.glieen.yibao.xizang.util;

import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/13 JDK8
 */
public class FileUtil {
    public static <T> List<T> parse(List<String[]> fileLines, Function<String[], T> function) {
        if (CollectionUtils.isEmpty(fileLines)) {
            return Collections.emptyList();
        }
        return fileLines.stream().map(function).collect(Collectors.toList());
    }
}
