package cn.glieen.yibao.xizang.util;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/13 JDK8
 */
public class YibaoException extends RuntimeException {
    private YibaoException(String message) {
        super(message);
    }

    public static YibaoException exception(String message) {
        return new YibaoException(message);
    }
}
