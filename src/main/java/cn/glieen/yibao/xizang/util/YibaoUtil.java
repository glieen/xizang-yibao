package cn.glieen.yibao.xizang.util;


import cn.glieen.yibao.xizang.api.Api;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.PageApi;
import cn.glieen.yibao.xizang.api.ResultApi;
import cn.glieen.yibao.xizang.api.impl.API9102;
import cn.glieen.yibao.xizang.pojo.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.ByteArrayBuffer;
import org.apache.http.util.EntityUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/6 JDK8
 * <p>
 * 请求工具
 */
@Slf4j
public class YibaoUtil {

    private static <R> R send(RequestObject request, Function<HttpEntity, R> function) {
        log.info("接口调用:{}", request);
        String requestBody = JsonUtil.toJson(request);
        CloseableHttpClient client = HttpClients.createDefault();

        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(YibaoProperties.connectionTimeout * 1000)
                .setSocketTimeout(YibaoProperties.socketTimeout * 1000)
                .build();

        HttpPost postRequest = new HttpPost(YibaoProperties.url + "/" + request.getInfno());

        postRequest.setConfig(requestConfig);

        Map<String, String> headers = createHeaders(request.getInfno());
        headers.forEach(postRequest::setHeader);
        HttpEntity body = new ByteArrayEntity(requestBody.getBytes(StandardCharsets.UTF_8), ContentType.APPLICATION_JSON);
        postRequest.setEntity(body);

        try (CloseableHttpResponse response = client.execute(postRequest)) {
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                postRequest.abort();
                throw YibaoException.exception(String.format("医保接口连接失败,错误码:%s", statusCode));
            }
            HttpEntity entity = response.getEntity();
            return function.apply(entity);
        } catch (IOException e) {
            throw YibaoException.exception("医保接口连接失败或超时");
        }
    }

    public static void send(Api api, String opter_no) {
        RequestObject request = createRequest(api, opter_no);
        send(request, YibaoUtil::getOutput);
    }

    public static Map<String, Object> getOutput(RequestObject request) {
        return send(request, YibaoUtil::getOutput);
    }

    public static <T> T getResult(ResultApi<T> api, RequestObject request) {
        return api.result(getOutput(request));
    }

    public static <T> T getResult(ResultApi<T> api, String opter_no) {
        RequestObject request = createRequest(api, opter_no);
        return api.result(getOutput(request));
    }

    public static <T> PageBean<T> getPage(PageApi<T> api, String opter_no) {
        RequestObject request = createRequest(api, opter_no);
        return api.page(getOutput(request));
    }

    public static List<String[]> getFileAsList(ResultApi<FileBean.Output> api, String opter_no) {
        FileBean.Output output = getResult(api, opter_no);

        API9102.Param param = new API9102.Param();
        param.setFilename("plc");
        param.setFixmedins_code(YibaoProperties.orgCode);
        param.setFile_qury_no(output.getFile_qury_no());
        API9102 downApi = new API9102(param);

        return getFileAsList(downApi, opter_no);
    }

    public static List<String[]> getFileAsList(API9102 api, String opter_no) {
        RequestObject request = YibaoUtil.createRequest(api, opter_no);
        String retString = YibaoUtil.getFileAsString(request);
        log.info("解析数据流并返回结果");
        return CommonUtil.splitStringAsList(retString);
    }

    public static RequestObject createRequest(Api api, String opter_no) {
        return createRequest(api, opter_no, false);
    }

    public static RequestObject createRequest(Api api, String opter_no, boolean isSign) {
        RequestObject request = new RequestObject();
        LocalDateTime now = LocalDateTime.now();
        SignInUser user = SignUtil.getSession(opter_no);
        if (user != null) {
            request.setOpter(user.getOpterNo());
            request.setSign_no(user.getSignNo());
            request.setOpter_name(user.getOpterName());
            request.setOpter_type(user.getOpterType().getCode());
        } else if (!isSign) {
            throw YibaoException.exception("当前用户未签到");
        }
        request.setInfno(api.getApiCode().getCode());
        request.setMsgid(createMsgId(now));
        request.setMdtrtarea_admvs(YibaoProperties.areaAdmvs);
        // TODO 需要处理异地业务办理
        request.setInsuplc_admdvs("");
        request.setDev_no("");
        request.setDev_safe_info("");
        request.setCainfo("");
        request.setSigntype("");
        request.setRecer_sys_code(YibaoProperties.sysCode);
        request.setInfver(YibaoProperties.infver);
        request.setInf_time(now);
        request.setFixmedins_code(YibaoProperties.orgCode);
        request.setFixmedins_name(YibaoProperties.orgName);
        List<ApiParam> paramList = api.getParamList();
        Map<String, Object> input = new HashMap<>();
        if (paramList != null) {
            for (ApiParam param : paramList) {
                input.put(param.paramName(), param.paramData());
            }
        }
        request.setInput(input);
        return request;
    }

    private synchronized static String createMsgId(LocalDateTime dateTime) {
        StringBuilder sb = new StringBuilder();
        String timeStr = DateTimeUtil.format(dateTime, "yyyyMMddHHmmss");
        sb.append(YibaoProperties.orgCode)
                .append(timeStr);
        Random random = new Random();
        for (int i = 0; i < 4; i++) {
            sb.append(random.nextInt(9) + 1);
        }
        return sb.toString();
    }

    private static String getFileAsString(RequestObject request) {
        return send(request, YibaoUtil::zip2String);
    }

    private static Map<String, String> createHeaders(String apiCode) {
        Map<String, String> signData = new HashMap<>();
        signData.put("_api_name", apiCode);
        signData.put("_api_version", YibaoProperties.apiVersion);
        signData.put("_api_access_key", YibaoProperties.apiAccessKey);
        signData.put("_api_timestamp", String.valueOf(System.currentTimeMillis()));
        signData.put("_api_signature", ReqSignUtil.generateSign(YibaoProperties.secretKey, signData));
        return signData;
    }

    private static Map<String, Object> getOutput(HttpEntity entity) {
        String json;
        try {
            json = EntityUtils.toString(entity, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw YibaoException.exception("获取接口响应失败");
        }
        if (StringUtils.isEmpty(json)) {
            return Collections.emptyMap();
        }
        log.info("医保请求结果响应:{}", json);
        Map<String, Object> resultMap = JsonUtil.toMap(json);
        if (resultMap.containsKey("code")) {
            throw YibaoException.exception(String.format("医保接口请求失败:%s", resultMap.get("message")));
        }
        ResponseObject response = JsonUtil.parse(json, ResponseObject.class);
        if (!response.success()) {
            throw YibaoException.exception(String.format("医保接口调用失败:%s", response.getErr_msg()));
        }
        return response.getOutput();
    }

    private static String zip2String(HttpEntity entity) {
        OutputStream os = new ByteArrayOutputStream();
        try {
            InputStream in = entity.getContent();
            if (in == null) {
                throw YibaoException.exception("未获取到数据流");
            }
            log.info("解压缩并读取数据");
            ZipInputStream zis = new ZipInputStream(in);
            ZipEntry entry = zis.getNextEntry();
            int temp;
            while (entry != null) {
                if (!entry.isDirectory()) {
                    while ((temp = zis.read()) != -1) {
                        os.write(temp);
                    }
                }
                entry = zis.getNextEntry();
            }
        } catch (IOException e) {
            // 如果数据流流读取异常，转换成Json处理
            getOutput(entity);
            throw YibaoException.exception("数据流读取异常");
        }
        return os.toString();
    }

    public static byte[] string2ZipByte(List<String[]> lines) {
        if (ObjectUtils.isEmpty(lines)) {
            throw YibaoException.exception("传输内容不能为空");
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < lines.size(); i++) {
            String[] line = lines.get(i);
            if (line == null) {
                continue;
            }
            for (int j = 0; j < line.length; j++) {
                sb.append(line[j]);
                if (j != line.length - 1) {
                    sb.append("\t");
                }
            }
            if (i != lines.size() - 1) {
                sb.append("\n");
            }
        }

        String fileName = String.valueOf(System.currentTimeMillis());

        String fullPath = System.getProperty("java.io.tmpdir") + fileName + ".zip";
        log.info("生成压缩文件:{}", fullPath);
        File file = new File(fullPath);

        try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(file, true), StandardCharsets.UTF_8)) {
            zos.putNextEntry(new ZipEntry(fileName + ".txt"));
            zos.write(sb.toString().getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw YibaoException.exception("数据流写入异常");
        }
        log.info("读取压缩文件:{}", fullPath);
        ByteArrayBuffer buffer = new ByteArrayBuffer(1000);
        try (FileInputStream fis = new FileInputStream(file)) {
            byte i;
            while ((i = ((byte) fis.read())) != -1) {
                buffer.append(i);
            }
        } catch (IOException e) {
            throw YibaoException.exception("数据流写入异常");
        }
        return buffer.toByteArray();
    }
}
