package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.ResultApi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/14 JDK8
 */
@AllArgsConstructor
public class API2001 implements ResultApi<API2001.Output> {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API2001;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 人员编号
         */
        private String psn_no;
        /**
         * 险种类型
         */
        private String insutype;
        /**
         * 定点医药机构编号
         */
        private String fixmedins_code;
        /**
         * 医疗类别
         */
        private String med_type;
        /**
         * 开始时间
         */
        private LocalDateTime begntime;
        /**
         * 结束时间
         */
        private LocalDateTime endtime;
        /**
         * 病种编码
         */
        private String dise_codg;
        /**
         * 病种名称
         */
        private String dise_name;
        /**
         * 手术操作代码
         */
        private String oprn_oprt_code;
        /**
         * 手术操作名称
         */
        private String oprn_oprt_name;
        /**
         * 生育类别
         */
        private String matn_type;
        /**
         * 计划生育手术类别
         */
        private String birctrl_type;

        @Override
        public String paramName() {
            return "data";
        }
    }

    @Data
    public static class Output {
        /**
         * 人员编号
         */
        private String psn_no;
        /**
         * 待遇检查类型
         */
        private String trt_chk_type;
        /**
         * 基金支付类型
         */
        private String fund_pay_type;
        /**
         * 基金款项待遇享受标志
         */
        private String trt_enjymnt_flag;
        /**
         * 开始日期
         */
        private LocalDate begndate;
        /**
         * 结束日期
         */
        private LocalDate enddate;
        /**
         * 待遇检查结果
         */
        private String trt_chk_rslt;
    }
}
