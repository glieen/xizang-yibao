package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.Api;
import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @since 2021/7/22 JDK8
 */
@AllArgsConstructor
public class API3502 implements Api {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API3502;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 医疗目录编码
         */
        private String med_list_codg;
        /**
         * 库存变更类型
         */
        private String inv_chg_type;
        /**
         * 定点医药机构目录编号
         */
        private String fixmedins_hilist_id;
        /**
         * 定点医药机构目录名称
         */
        private String fixmedins_hilist_name;
        /**
         * 定点医药机构批次流水号
         */
        private String fixmedins_bchno;
        /**
         * 单价
         */
        private BigDecimal pric;
        /**
         * 数量
         */
        private BigDecimal cnt;
        /**
         * 处方药标志
         */
        private String rx_flag;
        /**
         * 库存变更时间
         */
        private LocalDateTime inv_chg_time;
        /**
         * 库存变更经办人姓名
         */
        private String inv_chg_opter_name;
        /**
         * 备注
         */
        private String memo;
        /**
         * 拆零标志
         */
        private String trdn_flag;

        @Override
        public String paramName() {
            return "invinfo";
        }
    }
}
