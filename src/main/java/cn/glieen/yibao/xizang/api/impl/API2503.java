package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.ResultApi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/19 JDK8
 */
@AllArgsConstructor
public class API2503 implements ResultApi<API2503.Output> {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API2503;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {

        /**
         * 人员编号
         */
        private String psn_no;
        /**
         * 险种类型
         */
        private String insutype;
        /**
         * 门慢门特病种目录代码
         */
        private String opsp_dise_code;
        /**
         * 门慢门特病种名称
         */
        private String opsp_dise_name;
        /**
         * 联系电话
         */
        private String tel;
        /**
         * 联系地址
         */
        private String addr;
        /**
         * 参保机构医保区划
         */
        private String insu_optins;
        /**
         * 鉴定定点医药机构编号
         */
        private String ide_fixmedins_no;
        /**
         * 鉴定定点医药机构名称
         */
        private String ide_fixmedins_name;
        /**
         * 医院鉴定日期
         */
        private LocalDate hosp_ide_date;
        /**
         * 诊断医师编码
         */
        private String diag_dr_codg;
        /**
         * 诊断医师姓名
         */
        private String diag_dr_name;
        /**
         * 开始日期
         */
        private LocalDate begndate;
        /**
         * 结束日期
         */
        private LocalDate enddate;

        @Override
        public String paramName() {
            return "data";
        }
    }

    @Data
    public static class Output {
        /**
         * 输出
         */
        private Result result;
    }

    @Data
    public static class Result {
        /**
         * 待遇申报明细流水号
         */
        private String trt_dcla_detl_sn;
    }
}
