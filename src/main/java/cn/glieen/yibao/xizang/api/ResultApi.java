package cn.glieen.yibao.xizang.api;


import cn.glieen.yibao.xizang.util.CommonUtil;
import cn.glieen.yibao.xizang.util.JsonUtil;

import java.util.Map;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/7 JDK8
 * <p>
 * 结果接口
 */
public interface ResultApi<T> extends Api {
    default T result(Map<String, Object> output) {
        Class<T> clazz = CommonUtil.getGenericType(this.getClass(), ResultApi.class);
        return JsonUtil.toObject(output, clazz);
    }
}
