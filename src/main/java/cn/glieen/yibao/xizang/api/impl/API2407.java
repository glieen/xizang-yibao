package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.PageApi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/19 JDK8
 */
@AllArgsConstructor
public class API2407 implements PageApi<API2407.IptPsnSpFlagReg> {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API2407;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 分页条数
         */
        private Integer page_num;
        /**
         * 分页大小
         */
        private Integer page_size;
        /**
         * 住院人员特殊标识明细id
         */
        private String ipt_psn_sp_flag_detl_id;
        /**
         * 就诊ID
         */
        private String mdtrt_id;
        /**
         * 人员编号
         */
        private String psn_no;
        /**
         * 人员证件类型
         */
        private String psn_cert_type;
        /**
         * 证件号码
         */
        private String certno;
        /**
         * 人员姓名
         */
        private String psn_name;
        /**
         * 定点医药机构编号
         */
        private String fixmedins_code;
        /**
         * 定点医药机构名称
         */
        private String fixmedins_name;
        /**
         * 住院/门诊号
         */
        private String ipt_otp_no;
        /**
         * 住院人员特殊标识类型
         */
        private String ipt_psn_sp_flag_type;
        /**
         * 住院人员特殊标识
         */
        private String ipt_psn_sp_flag;
        /**
         * 医保区划
         */
        private String admdvs;

        @Override
        public String paramName() {
            return "QIptPsnSpFlagReg";
        }
    }

    @Data
    public static class IptPsnSpFlagReg {
        /**
         * 住院人员特殊标识明细id
         */
        private String ipt_psn_sp_flag_detl_id;
        /**
         * 就诊ID
         */
        private String mdtrt_id;
        /**
         * 人员编号
         */
        private String psn_no;
        /**
         * 人员证件类型
         */
        private String psn_cert_type;
        /**
         * 证件号码
         */
        private String certno;
        /**
         * 人员姓名
         */
        private String psn_name;
        /**
         * 定点医药机构编号
         */
        private String fixmedins_code;
        /**
         * 定点医药机构名称
         */
        private String fixmedins_name;
        /**
         * 住院/门诊号
         */
        private String ipt_otp_no;
        /**
         * 住院人员特殊标识类型字符型
         */
        private String ipt_psn_sp_flag_type;
        /**
         * 住院人员特殊标识
         */
        private String ipt_psn_sp_flag;
        /**
         * 医保区划
         */
        private String admdvs;
    }
}
