package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.ResultApi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/14 JDK8
 */
@AllArgsConstructor
public class API2102A implements ResultApi<API2102A.Output> {
    private final Param param;
    private final Param2 param2;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API2102A;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Arrays.asList(param, param2);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 人员编号
         */
        private String psn_no;
        /**
         * 就诊凭证类型
         */
        private String mdtrt_cert_type;
        /**
         * 就诊凭证编号
         */
        private String mdtrt_cert_no;
        /**
         * 开始时间
         */
        private String begntime;
        /**
         * 医疗费总额
         */
        private String medfee_sumamt;
        /**
         * 发票号
         */
        private String invono;
        /**
         * 险种类型
         */
        private String insutype;
        /**
         * 病种编码
         */
        private String dise_codg;
        /**
         * 病种名称
         */
        private String dise_name;
        /**
         * 个人账户使用标志
         */
        private String acct_used_flag;
        /**
         * 医疗类别
         */
        private String med_type;
        /**
         * 全自费金额
         */
        private BigDecimal fulamt_ownpay_amt;
        /**
         * 超限价自费费用
         */
        private BigDecimal overlmt_selfpay;
        /**
         * 先行自付金额
         */
        private BigDecimal preselfpay_amt;
        /**
         * 符合政策范围金额
         */
        private BigDecimal inscp_scp_amt;

        @Override
        public String paramName() {
            return "druginfo";
        }
    }

    @Data
    public static class Param2 implements ApiParam {
        private List<DrugDetail> data;

        @Override
        public String paramName() {
            return "drugdetail";
        }

        @Override
        public Object paramData() {
            return this.data;
        }
    }

    @Data
    public static class DrugDetail {
        /**
         * 费用明细流水号
         */
        private String feedetl_sn;
        /**
         * 处方号
         */
        private String rxno;
        /**
         * 外购处方标志
         */
        private String rx_circ_flag;
        /**
         * 费用发生时间
         */
        private LocalDateTime fee_ocur_time;
        /**
         * 医疗目录编码
         */
        private String med_list_codg;
        /**
         * 医药机构目录编码
         */
        private String medins_list_codg;
        /**
         * 明细项目费用总额
         */
        private BigDecimal det_item_fee_sumamt;
        /**
         * 数量
         */
        private BigDecimal cnt;
        /**
         * 单价
         */
        private BigDecimal pric;
        /**
         * 单次剂量描述
         */
        private String sin_dos_dscr;
        /**
         * 使用频次描述
         */
        private String used_frqu_dscr;
        /**
         * 周期天数
         */
        private BigDecimal prd_days;
        /**
         * 用药途径描述
         */
        private String medc_way_dscr;
        /**
         * 开单医生编码
         */
        private String bilg_dr_codg;
        /**
         * 开单医师姓名
         */
        private String bilg_dr_name;
        /**
         * 中药使用方式
         */
        private String tcmdrug_used_way;
    }

    @Data
    public static class Output {
        /**
         * 结算信息
         */
        private Setlinfo setlinfo;
        /**
         * 结算基金分项信息
         */
        private List<Setldetail> setldetail;
        /**
         * 明细分割信息
         */
        private List<Detlcutinfo> detlcutinfo;
    }

    @Data
    public static class Setlinfo {
        /**
         * 结算ID
         */
        private String setl_id;
        /**
         * 就诊ID
         */
        private String mdtrt_id;
        /**
         * 人员编号
         */
        private String psn_no;
        /**
         * 人员姓名
         */
        private String psn_name;
        /**
         * 人员证件类型
         */
        private String psn_cert_type;
        /**
         * 证件号码
         */
        private String certno;
        /**
         * 性别
         */
        private String gend;
        /**
         * 民族
         */
        private String naty;
        /**
         * 出生日期
         */
        private LocalDate brdy;
        /**
         * 年龄
         */
        private BigDecimal age;
        /**
         * 险种类型
         */
        private String insutype;
        /**
         * 人员类别
         */
        private String psn_type;
        /**
         * 公务员标志
         */
        private String cvlserv_flag;
        /**
         * 结算时间
         */
        private LocalDateTime setl_time;
        /**
         * 就诊凭证类型
         */
        private String mdtrt_cert_type;
        /**
         * 医疗类别
         */
        private String med_type;
        /**
         * 医疗费总额
         */
        private BigDecimal medfee_sumamt;
        /**
         * 全自费金额
         */
        private BigDecimal fulamt_ownpay_amt;
        /**
         * 超限价自费费用
         */
        private BigDecimal overlmt_selfpay;
        /**
         * 先行自付金额
         */
        private BigDecimal preselfpay_amt;
        /**
         * 符合政策范围金额
         */
        private BigDecimal inscp_scp_amt;
        /**
         * 实际支付起付线
         */
        private BigDecimal act_pay_dedc;
        /**
         * 基本医疗保险统筹基金支出
         */
        private BigDecimal hifp_pay;
        /**
         * 基本医疗保险统筹基金支付比例
         */
        private BigDecimal pool_prop_selfpay;
        /**
         * 公务员医疗补助资金支出
         */
        private BigDecimal cvlserv_pay;
        /**
         * 企业补充医疗保险基金支出
         */
        private BigDecimal hifes_pay;
        /**
         * 居民大病保险资金支出
         */
        private BigDecimal hifmi_pay;
        /**
         * 职工大额医疗费用补助基金支出
         */
        private BigDecimal hifob_pay;
        /**
         * 医疗救助基金支出
         */
        private BigDecimal maf_pay;
        /**
         * 其他支出
         */
        private BigDecimal oth_pay;
        /**
         * 基金支付总额
         */
        private BigDecimal fund_pay_sumamt;
        /**
         * 个人负担总金额
         */
        private BigDecimal psn_part_amt;
        /**
         * 个人账户支出
         */
        private BigDecimal acct_pay;
        /**
         * 个人现金支出
         */
        private BigDecimal psn_cash_pay;
        /**
         * 余额
         */
        private BigDecimal balc;
        /**
         * 个人账户共济支付金额
         */
        private BigDecimal acct_mulaid_pay;
        /**
         * 医药机构结算
         */
        private String medins_setl_id;
        /**
         * 清算经办机构
         */
        private String clr_optins;
        /**
         * 清算方式
         */
        private String clr_way;
        /**
         * 清算类别
         */
        private String clr_type;
    }

    @Data
    public static class Setldetail {
        /**
         * 基金支付类型
         */
        private String fund_pay_type;
        /**
         * 符合政策范围金额
         */
        private String inscp_scp_amt;
        /**
         * 本次可支付限额金额
         */
        private String crt_payb_lmt_amt;
        /**
         * 基金支付金额
         */
        private String fund_payamt;
        /**
         * 基金支付类型名称
         */
        private String fund_pay_type_name;
        /**
         * 结算过程信息
         */
        private String setl_proc_info;
    }

    @Data
    public static class Detlcutinfo {
        /**
         * 费用明细流水号
         */
        private String feedetl_sn;
        /**
         * 明细项目费用总额
         */
        private BigDecimal det_item_fee_sumamt;
        /**
         * 数量
         */
        private BigDecimal cnt;
        /**
         * 单价
         */
        private BigDecimal pric;
        /**
         * 定价上限金额
         */
        private BigDecimal pric_uplmt_amt;
        /**
         * 自付比例
         */
        private BigDecimal selfpay_prop;
        /**
         * 全自费金额
         */
        private BigDecimal fulamt_ownpay_amt;
        /**
         * 超限价金额
         */
        private BigDecimal overlmt_amt;
        /**
         * 先行自付金额
         */
        private BigDecimal preselfpay_amt;
        /**
         * 符合政策范围金额
         */
        private BigDecimal inscp_scp_amt;
        /**
         * 收费项目等级
         */
        private String chrgitm_lv;
        /**
         * 医疗收费项目类别
         */
        private String med_chrgitm_type;
        /**
         * 基本药物标志
         */
        private String bas_medn_flag;
        /**
         * 医保谈判药品标志
         */
        private String hi_nego_drug_flag;
        /**
         * 儿童用药标志
         */
        private String chld_medc_flag;
        /**
         * 目录特项标志
         */
        private String list_sp_item_flag;
        /**
         * 直报标志
         */
        private String drt_reim_flag;
        /**
         * 备注
         */
        private String memo;
    }
}
