package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.PageApi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/14 JDK8
 */
@AllArgsConstructor
public class API1319 implements PageApi<API1319.Record> {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API1319;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 查询时间点
         */
        private LocalDate query_date;
        /**
         * 医保目录编码
         */
        private String hilist_code;
        /**
         * 医保目录自付比例人员类别
         */
        private String selfpay_prop_psn_type;
        /**
         * 目录自付比例类别
         */
        private String selfpay_prop_type;
        /**
         * 参保机构医保区划
         */
        private String insu_admdvs;
        /**
         * 开始日期
         */
        private LocalDate begndate;
        /**
         * 结束日期
         */
        private LocalDate enddate;
        /**
         * 有效标志
         */
        private String vali_flag;
        /**
         * 唯一记录号
         */
        private String rid;
        /**
         * 表名
         */
        private String tabname;
        /**
         * 统筹区
         */
        private String poolarea_no;
        /**
         * 更新时间
         */
        private LocalDate updt_time;
        /**
         * 当前页数
         */
        private Integer page_num;
        /**
         * 本页数据量
         */
        private Integer page_size;

        @Override
        public String paramName() {
            return "data";
        }
    }

    @Data
    public static class Record {
        /**
         * 医保目录编码
         */
        private String hilist_code;
        /**
         * 医保目录自付比例人员类别
         */
        private String selfpay_prop_psn_type;
        /**
         * 目录自付比例类别
         */
        private String selfpay_prop_type;
        /**
         * 参保机构医保区划
         */
        private String insu_admdvs;
        /**
         * 开始日期
         */
        private LocalDate begndate;
        /**
         * 结束日期
         */
        private LocalDate enddate;
        /**
         * 自付比例
         */
        private BigDecimal selfpay_prop;
        /**
         * 有效标志
         */
        private String vali_flag;
        /**
         * 唯一记录号
         */
        private String rid;
        /**
         * 更新时间
         */
        private LocalDate updt_time;
        /**
         * 创建人
         */
        private String crter_id;
        /**
         * 创建人姓名
         */
        private String crter_name;
        /**
         * 创建时间
         */
        private LocalDate crte_time;
        /**
         * 创建机构
         */
        private String crte_optins_no;
        /**
         * 经办人
         */
        private String opter_id;
        /**
         * 经办人姓名
         */
        private String opter_name;
        /**
         * 经办时间
         */
        private LocalDate opt_time;
        /**
         * 经办机构
         */
        private String optins_no;
        /**
         * 表名
         */
        private String tabname;
        /**
         * 统筹区
         */
        private String poolarea_no;
    }
}
