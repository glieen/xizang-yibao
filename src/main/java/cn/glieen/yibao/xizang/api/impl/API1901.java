package cn.glieen.yibao.xizang.api.impl;


import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.ResultApi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/12 JDK8
 */
@AllArgsConstructor
public class API1901 implements ResultApi<API1901.Output> {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API1901;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 字典类型
         */
        private String type;
        /**
         * 父字典键值
         */
        private String parentValue;
        /**
         * 行政区划
         */
        private String admdvs;
        /**
         * 查询日期
         */
        private LocalDate date;
        /**
         * 有效标志
         */
        private String valiFlag;

        @Override
        public String paramName() {
            return "data";
        }
    }

    @Data
    public static class Output {
        /**
         * 字典表信息
         */
        private List<DictList> list;
    }

    @Data
    public static class DictList {
        /**
         * 字典类型
         */
        private String type;
        /**
         * 字典标签
         */
        private String label;
        /**
         * 字典键值
         */
        private String value;
        /**
         * 父字典键值
         */
        private String parentValue;
        /**
         * 序号
         */
        private Integer sort;
        /**
         * 权限标识
         */
        private String valiFlag;
        /**
         * 创建账户
         */
        private String createUser;
        /**
         * 创建时间
         */
        private LocalDateTime createDate;
        /**
         * 版本号
         */
        private Integer version;
    }
}
