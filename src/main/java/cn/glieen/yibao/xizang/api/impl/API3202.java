package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.ResultApi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @since 2021/7/20 JDK8
 */
@AllArgsConstructor
public class API3202 implements ResultApi<API3202.Output> {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API3202;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {

        /**
         * 结算经办机构
         */
        private String setl_optins;
        /**
         * 文件查询号
         */
        private String file_qury_no;
        /**
         * 对账开始日期
         */
        private LocalDate stmt_begndate;
        /**
         * 对账结束日期
         */
        private LocalDate stmt_enddate;
        /**
         * 医疗费总额
         */
        private BigDecimal medfee_sumamt;
        /**
         * 基金支付总额
         */
        private BigDecimal fund_pay_sumamt;
        /**
         * 现金支付金额
         */
        private BigDecimal cash_payamt;
        /**
         * 定点医药机构结算笔数
         */
        private Integer fixmedins_setl_cnt;

        @Override
        public String paramName() {
            return "data";
        }
    }


    @Data
    public static class Output {
        /**
         * 输出
         */
        private Fileinfo fileinfo;
    }

    @Data
    public static class Fileinfo {
        /**
         * 文件查询号
         */
        private String file_qury_no;
        /**
         * 文件名称
         */
        private String filename;
        /**
         * 下载截止时间
         */
        private LocalDateTime dld_endtime;
    }

}

