package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.ResultApi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/19 JDK8
 */
@AllArgsConstructor
public class API3201 implements ResultApi<API3201.Output> {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API3201;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 险种
         */
        private String insutype;
        /**
         * 清算类别
         */
        private String clr_type;
        /**
         * 结算经办机构
         */
        private String setl_optins;
        /**
         * 对账开始日期
         */
        private LocalDate stmt_begndate;
        /**
         * 对账结束日期
         */
        private LocalDate stmt_enddate;
        /**
         * 医疗费总额
         */
        private BigDecimal medfee_sumamt;
        /**
         * 基金支付总额
         */
        private BigDecimal fund_pay_sumamt;
        /**
         * 个人账户支付金额
         */
        private BigDecimal acct_pay;
        /**
         * 定点医药机构结算笔数
         */
        private Integer fixmedins_setl_cnt;

        @Override
        public String paramName() {
            return "data";
        }
    }

    @Data
    public static class Output {
        /**
         * 结算经办机构
         */
        private String setl_optins;
        /**
         * 对账结果
         */
        private String stmt_rslt;
        /**
         * 对账结果说明
         */
        private String stmt_rslt_dscr;
    }

}
