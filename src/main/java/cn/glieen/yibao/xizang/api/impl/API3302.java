package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.Api;
import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @since 2021/7/20 JDK8
 */
@AllArgsConstructor
public class API3302 implements Api {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API3302;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 定点医药机构编号
         */
        private String fixmedins_code;
        /**
         * 定点医药机构目录编号
         */
        private String fixmedins_hilist_id;
        /**
         * 目录类别
         */
        private String list_type;
        /**
         * 医疗目录编码
         */
        private String med_list_codg;

        @Override
        public String paramName() {
            return "data";
        }
    }
}
