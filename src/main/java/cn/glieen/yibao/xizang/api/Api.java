package cn.glieen.yibao.xizang.api;

import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/7 JDK8
 * <p>
 * 接口
 */
public interface Api {
    ApiCode getApiCode();

    List<ApiParam> getParamList();
}
