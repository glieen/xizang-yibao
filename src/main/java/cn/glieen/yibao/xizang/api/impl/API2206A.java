package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.ResultApi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/16 JDK8
 */
@AllArgsConstructor
public class API2206A implements ResultApi<API2206A.Output> {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API2206A;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 人员编号
         */
        private String psn_no;
        /**
         * 就诊凭证类型
         */
        private String mdtrt_cert_type;
        /**
         * 就诊凭证编号
         */
        private String mdtrt_cert_no;
        /**
         * 医疗类别
         */
        private String med_type;
        /**
         * 医疗费总额
         */
        private BigDecimal medfee_sumamt;
        /**
         * 个人结算方式
         */
        private String psn_setlway;
        /**
         * 就诊ID
         */
        private String mdtrt_id;
        /**
         * 收费批次号
         */
        private String chrg_bchno;
        /**
         * 个人账户使用标志
         */
        private String acct_used_flag;
        /**
         * 险种类型
         */
        private String insutype;

        @Override
        public String paramName() {
            return "data";
        }
    }

    @Data
    public static class Output {
        /**
         * 结算信息
         */
        private Setlinfo setlinfo;
        /**
         * 结算基金分项信息
         */
        private List<Setldetail> setldetail;
    }

    @Data
    public static class Setlinfo {
        /**
         * 就诊ID
         */
        private String mdtrt_id;
        /**
         * 人员编号
         */
        private String psn_no;
        /**
         * 人员姓名
         */
        private String psn_name;
        /**
         * 人员证件类型
         */
        private String psn_cert_type;
        /**
         * 证件号码
         */
        private String certno;
        /**
         * 性别
         */
        private String gend;
        /**
         * 民族
         */
        private String naty;
        /**
         * 出生日期
         */
        private String brdy;
        /**
         * 年龄
         */
        private BigDecimal age;
        /**
         * 险种类型
         */
        private String insutype;
        /**
         * 人员类别
         */
        private String psn_type;
        /**
         * 公务员标志
         */
        private String cvlserv_flag;
        /**
         * 结算时间
         */
        private LocalDateTime setl_time;
        /**
         * 就诊凭证类型
         */
        private String mdtrt_cert_type;
        /**
         * 医疗类别
         */
        private String med_type;
        /**
         * 医疗费总额
         */
        private BigDecimal medfee_sumamt;
        /**
         * 全自费金额
         */
        private BigDecimal fulamt_ownpay_amt;
        /**
         * 超限价自费费用
         */
        private BigDecimal overlmt_selfpay;
        /**
         * 先行自付金额
         */
        private BigDecimal preselfpay_amt;
        /**
         * 符合政策范围金额
         */
        private BigDecimal inscp_scp_amt;
        /**
         * 实际支付起付线
         */
        private BigDecimal act_pay_dedc;
        /**
         * 基本医疗保险统筹基金支出
         */
        private BigDecimal hifp_pay;
        /**
         * 基本医疗保险统筹基金支付比例
         */
        private BigDecimal pool_prop_selfpay;
        /**
         * 公务员医疗补助资金支出
         */
        private BigDecimal cvlserv_pay;
        /**
         * 企业补充医疗保险基金支出
         */
        private BigDecimal hifes_pay;
        /**
         * 居民大病保险资金支出
         */
        private BigDecimal hifmi_pay;
        /**
         * 职工大额医疗费用补助基金支出
         */
        private BigDecimal hifob_pay;
        /**
         * 医疗救助基金支出
         */
        private BigDecimal maf_pay;
        /**
         * 其他支出
         */
        private BigDecimal oth_pay;
        /**
         * 基金支付总额
         */
        private BigDecimal fund_pay_sumamt;
        /**
         * 个人负担总金额
         */
        private BigDecimal psn_part_amt;
        /**
         * 个人账户支出
         */
        private BigDecimal acct_pay;
        /**
         * 个人现金支出
         */
        private BigDecimal psn_cash_pay;
        /**
         * 医院负担金额
         */
        private BigDecimal hosp_part_amt;
        /**
         * 余额
         */
        private BigDecimal balc;
        /**
         * 个人账户共济支付金额
         */
        private BigDecimal acct_mulaid_pay;
        /**
         * 医药机构结算ID
         */
        private String medins_setl_id;
        /**
         * 清算经办机构
         */
        private String clr_optins;
        /**
         * 清算方式
         */
        private String clr_way;
        /**
         * 清算类别
         */
        private String clr_type;
    }

    @Data
    public static class Setldetail {
        /**
         * 基金支付类型
         */
        private String fund_pay_type;
        /**
         * 符合政策范围金额
         */
        private BigDecimal inscp_scp_amt;
        /**
         * 本次可支付限额金额
         */
        private BigDecimal crt_payb_lmt_amt;
        /**
         * 基金支付金额
         */
        private BigDecimal fund_payamt;
        /**
         * 基金支付类型名称
         */
        private String fund_pay_type_name;
        /**
         * 结算过程信息
         */
        private String setl_proc_info;
    }
}
