package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.ResultApi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/16 JDK8
 */
@AllArgsConstructor
public class API2305 implements ResultApi<API2305.Output> {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API2305;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 结算ID
         */
        private String setl_id;
        /**
         * 就诊ID
         */
        private String mdtrt_id;
        /**
         * 人员编号
         */
        private String psn_no;

        @Override
        public String paramName() {
            return "data";
        }
    }

    @Data
    public static class Output {
        /**
         * 结算信息
         */
        private Setlinfo setlinfo;
        /**
         * 结算基金分项信息
         */
        private List<Setldetail> setldetail;
    }

    @Data
    public static class Setlinfo {
        /**
         * 就诊ID
         */
        private String mdtrt_id;
        /**
         * 结算ID
         */
        private String setl_id;
        /**
         * 清算经办机构
         */
        private String clr_optins;
        /**
         * 结算时间
         */
        private LocalDateTime setl_time;
        /**
         * 医疗费总额
         */
        private BigDecimal medfee_sumamt;
        /**
         * 全自费金额
         */
        private BigDecimal fulamt_ownpay_amt;
        /**
         * 超限价自费费用
         */
        private BigDecimal overlmt_selfpay;
        /**
         * 先行自付金额
         */
        private BigDecimal preselfpay_amt;
        /**
         * 符合政策范围金额
         */
        private BigDecimal inscp_scp_amt;
        /**
         * 实际支付起付线
         */
        private BigDecimal act_pay_dedc;
        /**
         * 基本医疗保险统筹基金支出
         */
        private BigDecimal hifp_pay;
        /**
         * 基本医疗保险统筹基金支付比例
         */
        private BigDecimal pool_prop_selfpay;
        /**
         * 公务员医疗补助资金支出
         */
        private BigDecimal cvlserv_pay;
        /**
         * 企业补充医疗保险基金支出
         */
        private BigDecimal hifes_pay;
        /**
         * 居民大病保险资金支出
         */
        private BigDecimal hifmi_pay;
        /**
         * 职工大额医疗费用补助基金支出
         */
        private BigDecimal hifob_pay;
        /**
         * 医疗救助基金支出
         */
        private BigDecimal maf_pay;
        /**
         * 其他支出
         */
        private BigDecimal oth_pay;
        /**
         * 基金支付总额
         */
        private BigDecimal fund_pay_sumamt;
        /**
         * 个人负担总金额
         */
        private BigDecimal psn_part_amt;
        /**
         * 个人账户支出
         */
        private BigDecimal acct_pay;
        /**
         * 个人现金支出
         */
        private BigDecimal psn_cash_pay;
        /**
         * 医院负担金额
         */
        private BigDecimal hosp_part_amt;
        /**
         * 余额
         */
        private BigDecimal balc;
        /**
         * 个人账户共济支付金额
         */
        private BigDecimal acct_mulaid_pay;
        /**
         * 医药机构结算ID
         */
        private String medins_setl_id;
        /**
         * 伤残人员医疗保障基金支出
         */
        private BigDecimal hifdm_pay;
    }

    @Data
    public static class Setldetail {
        /**
         * 基金支付类型
         */
        private String fund_pay_type;
        /**
         * 符合政策范围金额
         */
        private BigDecimal inscp_scp_amt;
        /**
         * 本次可支付限额金额
         */
        private BigDecimal crt_payb_lmt_amt;
        /**
         * 基金支付金额
         */
        private BigDecimal fund_payamt;
        /**
         * 基金支付类型名称
         */
        private String fund_pay_type_name;
        /**
         * 结算过程信息
         */
        private String setl_proc_info;
    }
}
