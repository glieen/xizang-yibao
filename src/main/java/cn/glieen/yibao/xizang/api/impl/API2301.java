package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.ResultApi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/16 JDK8
 */
@AllArgsConstructor
public class API2301 implements ResultApi<API2301.Output> {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API2301;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 费用明细列表
         */
        private List<Feedetail> data;

        @Override
        public String paramName() {
            return "feedetail";
        }

        @Override
        public Object paramData() {
            return this.data;
        }
    }

    @Data
    public static class Feedetail {
        /**
         * 费用明细流水号
         */
        private String feedetl_sn;
        /**
         * 原费用流水号
         */
        private String init_feedetl_sn;
        /**
         * 就诊ID
         */
        private String mdtrt_id;
        /**
         * 医嘱号
         */
        private String drord_no;
        /**
         * 人员编号
         */
        private String psn_no;
        /**
         * 医疗类别
         */
        private String med_type;
        /**
         * 费用发生时间
         */
        private LocalDateTime fee_ocur_time;
        /**
         * 医疗目录编码
         */
        private String med_list_codg;
        /**
         * 医药机构目录编码
         */
        private String medins_list_codg;
        /**
         * 明细项目费用总额
         */
        private BigDecimal det_item_fee_sumamt;
        /**
         * 数量
         */
        private BigDecimal cnt;
        /**
         * 单价
         */
        private BigDecimal pric;
        /**
         * 开单科室编码
         */
        private String bilg_dept_codg;
        /**
         * 开单科室名称
         */
        private String bilg_dept_name;
        /**
         * 开单医生编码
         */
        private String bilg_dr_codg;
        /**
         * 开单医师姓名
         */
        private String bilg_dr_name;
        /**
         * 受单科室编码
         */
        private String acord_dept_codg;
        /**
         * 受单科室名称
         */
        private String acord_dept_name;
        /**
         * 受单医生编码
         */
        private String orders_dr_code;
        /**
         * 受单医生姓名
         */
        private String orders_dr_name;
        /**
         * 医院审批标志
         */
        private String hosp_appr_flag;
        /**
         * 中药使用方式
         */
        private String tcmdrug_used_way;
        /**
         * 外检标志
         */
        private String etip_flag;
        /**
         * 外检医院编码
         */
        private String etip_hosp_code;
        /**
         * 出院带药标志
         */
        private String dscg_tkdrug_flag;
        /**
         * 生育费用标志
         */
        private String matn_fee_flag;
        /**
         * 备注
         */
        private String memo;
    }

    @Data
    public static class Output {
        /**
         * 结果
         */
        private List<Result> result;
    }

    @Data
    public static class Result {
        /**
         * 费用明细流水号
         */
        private String feedetl_sn;
        /**
         * 明细项目费用总额
         */
        private BigDecimal det_item_fee_sumamt;
        /**
         * 数量
         */
        private BigDecimal cnt;
        /**
         * 单价
         */
        private BigDecimal pric;
        /**
         * 定价上限金额
         */
        private BigDecimal pric_uplmt_amt;
        /**
         * 自付比例
         */
        private BigDecimal selfpay_prop;
        /**
         * 全自费金额
         */
        private BigDecimal fulamt_ownpay_amt;
        /**
         * 超限价金额
         */
        private BigDecimal overlmt_amt;
        /**
         * 先行自付金额
         */
        private BigDecimal preselfpay_amt;
        /**
         * 符合政策范围金额
         */
        private BigDecimal inscp_scp_amt;
        /**
         * 收费项目等级
         */
        private String chrgitm_lv;
        /**
         * 医疗收费项目类别
         */
        private String med_chrgitm_type;
        /**
         * 基本药物标志
         */
        private String bas_medn_flag;
        /**
         * 医保谈判药品标志
         */
        private String hi_nego_drug_flag;
        /**
         * 儿童用药标志
         */
        private String chld_medc_flag;
        /**
         * 目录特项标志
         */
        private String list_sp_item_flag;
        /**
         * 限制使用标志
         */
        private String lmt_used_flag;
        /**
         * 直报标志
         */
        private String drt_reim_flag;
        /**
         * 备注
         */
        private String memo;
    }

}
