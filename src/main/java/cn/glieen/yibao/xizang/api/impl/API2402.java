package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.Api;
import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/19 JDK8
 */
@AllArgsConstructor
public class API2402 implements Api {
    private final Param param;
    private final Param2 param2;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API2402;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Arrays.asList(param, param2);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 就诊ID
         */
        private String mdtrt_id;
        /**
         * 人员编号
         */
        private String psn_no;
        /**
         * 险种类型
         */
        private String insutype;
        /**
         * 结束时间
         */
        private LocalDateTime endtime;
        /**
         * 病种编码
         */
        private String dise_codg;
        /**
         * 病种名称
         */
        private String dise_name;
        /**
         * 手术操作代码
         */
        private String oprn_oprt_code;
        /**
         * 手术操作名称
         */
        private String oprn_oprt_name;
        /**
         * 计划生育服务证号
         */
        private String fpsc_no;
        /**
         * 生育类别
         */
        private String matn_type;
        /**
         * 计划生育手术类别
         */
        private String birctrl_type;
        /**
         * 晚育标志
         */
        private String latechb_flag;
        /**
         * 孕周数
         */
        private Integer geso_val;
        /**
         * 胎次
         */
        private Integer fetts;
        /**
         * 胎儿数
         */
        private Integer fetus_cnt;
        /**
         * 早产标志
         */
        private String pret_flag;
        /**
         * 计划生育手术或生育日期
         */
        private LocalDate birctrl_matn_date;
        /**
         * 伴有并发症标志
         */
        private String cop_flag;
        /**
         * 出院科室编码
         */
        private String dscg_dept_codg;
        /**
         * 出院科室名称
         */
        private String dscg_dept_name;
        /**
         * 出院床位
         */
        private String dscg_bed;
        /**
         * 离院方式
         */
        private String dscg_way;
        /**
         * 死亡日期
         */
        private LocalDate die_date;

        @Override
        public String paramName() {
            return "dscginfo";
        }
    }

    @Data
    public static class Param2 implements ApiParam {

        /**
         * 出院诊断信息
         */
        private List<Diseinfo> data;

        @Override
        public String paramName() {
            return "diseinfo";
        }

        @Override
        public Object paramData() {
            return this.data;
        }
    }

    @Data
    public static class Diseinfo {
        /**
         * 就诊ID
         */
        private String mdtrt_id;
        /**
         * 人员编号
         */
        private String psn_no;
        /**
         * 诊断类别
         */
        private String diag_type;
        /**
         * 主诊断标志
         */
        private String maindiag_flag;
        /**
         * 诊断排序号
         */
        private Integer diag_srt_no;
        /**
         * 诊断代码
         */
        private String diag_code;
        /**
         * 诊断名称
         */
        private String diag_name;
        /**
         * 诊断科室
         */
        private String diag_dept;
        /**
         * 诊断医生编码
         */
        private String dise_dor_no;
        /**
         * 诊断医生姓名
         */
        private String dise_dor_name;
        /**
         * 诊断时间
         */
        private LocalDateTime diag_time;
    }
}
