package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.Api;
import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/19 JDK8
 */
@AllArgsConstructor
public class API2502 implements Api {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API2502;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 待遇申报明细流水号
         */
        private String trt_dcla_detl_sn;
        /**
         * 人员编号
         */
        private String psn_no;
        /**
         * 备注
         */
        private String memo;

        @Override
        public String paramName() {
            return "data";
        }
    }

}
