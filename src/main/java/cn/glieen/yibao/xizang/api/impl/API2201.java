package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.ResultApi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/16 JDK8
 */
@AllArgsConstructor
public class API2201 implements ResultApi<API2201.Output> {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API2201;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 人员编号
         */
        private String psn_no;
        /**
         * 险种类型
         */
        private String insutype;
        /**
         * 开始时间
         */
        private LocalDateTime begntime;
        /**
         * 就诊凭证类型
         */
        private String mdtrt_cert_type;
        /**
         * 就诊凭证编号
         */
        private String mdtrt_cert_no;
        /**
         * 住院/门诊号
         */
        private String ipt_otp_no;
        /**
         * 医师编码
         */
        private String atddr_no;
        /**
         * 医师姓名
         */
        private String dr_name;
        /**
         * 科室编码
         */
        private String dept_code;
        /**
         * 科室名称
         */
        private String dept_name;
        /**
         * 科别
         */
        private String caty;

        @Override
        public String paramName() {
            return "data";
        }
    }

    @Data
    public static class Output {
        /**
         * 返回结果
         */
        private RetData data;
    }

    @Data
    public static class RetData {
        /**
         * 就诊ID
         */
        private String mdtrt_id;
        /**
         * 人员编号
         */
        private String psn_no;
        /**
         * 住院/门诊号
         */
        private String ipt_otp_no;
    }

}
