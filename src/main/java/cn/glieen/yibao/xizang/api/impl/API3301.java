package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.Api;
import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @since 2021/7/20 JDK8
 */
@AllArgsConstructor
public class API3301 implements Api {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API3301;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {

        private List<ReqData> data;

        @Override
        public String paramName() {
            return "data";
        }

        @Override
        public Object paramData() {
            return this.data;
        }
    }

    @Data
    public static class ReqData {
        /**
         * 定点医药机构目录编号
         */
        private String fixmedins_hilist_id;
        /**
         * 定点医药机构目录名称
         */
        private String fixmedins_hilist_name;
        /**
         * 目录类别
         */
        private String list_type;
        /**
         * 医疗目录编码
         */
        private String med_list_codg;
        /**
         * 开始日期
         */
        private LocalDate begndate;
        /**
         * 结束日期
         */
        private LocalDate enddate;
        /**
         * 批准文号
         */
        private String aprvno;
        /**
         * 剂型
         */
        private String dosform;
        /**
         * 除外内容
         */
        private String exct_cont;
        /**
         * 项目内涵
         */
        private String item_cont;
        /**
         * 计价单位
         */
        private String prcunt;
        /**
         * 规格
         */
        private String spec;
        /**
         * 包装规格
         */
        private String pacspec;
        /**
         * 备注
         */
        private String memo;
    }


}
