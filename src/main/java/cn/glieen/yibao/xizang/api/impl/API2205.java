package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.Api;
import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/16 JDK8
 */
@AllArgsConstructor
public class API2205 implements Api {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API2205;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 就诊ID
         */
        private String mdtrt_id;
        /**
         * 收费批次号
         */
        private String chrg_bchno;
        /**
         * 人员编号
         */
        private String psn_no;

        @Override
        public String paramName() {
            return "data";
        }
    }

}
