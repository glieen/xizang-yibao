package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.Api;
import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/16 JDK8
 */
@AllArgsConstructor
public class API2302 implements Api {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API2302;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        private List<ParamData> data;

        @Override
        public String paramName() {
            return "data";
        }

        @Override
        public Object paramData() {
            return this.data;
        }
    }

    @Data
    public static class ParamData {
        /**
         * 费用明细流水号
         */
        private String feedetl_sn;
        /**
         * 就诊ID
         */
        private String mdtrt_id;
        /**
         * 人员编号
         */
        private String psn_no;
    }

}
