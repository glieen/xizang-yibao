package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.Api;
import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @since 2021/7/20 JDK8
 */
@AllArgsConstructor
public class API3401 implements Api {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API3401;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {

        /**
         * 医院科室编码
         */
        private String hosp_dept_codg;
        /**
         * 科别
         */
        private String caty;
        /**
         * 医院科室名称
         */
        private String hosp_dept_name;
        /**
         * 开始时间
         */
        private LocalDateTime begntime;
        /**
         * 结束时间
         */
        private LocalDateTime endtime;
        /**
         * 简介
         */
        private String itro;
        /**
         * 科室负责人姓名
         */
        private String dept_resper_name;
        /**
         * 科室负责人电话
         */
        private String dept_resper_tel;
        /**
         * 科室医疗服务范围
         */
        private String dept_med_serv_scp;
        /**
         * 科室成立日期
         */
        private LocalDateTime dept_estbdat;
        /**
         * 批准床位数量
         */
        private Integer aprv_bed_cnt;
        /**
         * 医保认可床位数
         */
        private Integer hi_crtf_bed_cnt;
        /**
         * 统筹区编号
         */
        private String poolarea_no;
        /**
         * 医师人数
         */
        private Integer dr_psncnt;
        /**
         * 药师人数
         */
        private Integer phar_psncnt;
        /**
         * 护士人数
         */
        private Integer nurs_psncnt;
        /**
         * 技师人数
         */
        private Integer tecn_psncnt;
        /**
         * 备注
         */
        private String memo;

        @Override
        public String paramName() {
            return "deptinfo";
        }
    }
}
