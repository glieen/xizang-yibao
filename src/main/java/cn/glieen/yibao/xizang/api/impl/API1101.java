package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.ResultApi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/12 JDK8
 */
@AllArgsConstructor
public class API1101 implements ResultApi<API1101.Output> {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API1101;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 就诊凭证类型
         */
        private String mdtrt_cert_type;
        /**
         * 就诊凭证编号
         */
        private String mdtrt_cert_no;
        /**
         * 卡识别码
         */
        private String card_sn;
        /**
         * 开始时间
         */
        private LocalDateTime begntime;
        /**
         * 人员证件类型
         */
        private String psn_cert_type;
        /**
         * 证件号码
         */
        private String certno;
        /**
         * 人员姓名
         */
        private String psn_name;

        @Override
        public String paramName() {
            return "data";
        }
    }

    @Data
    public static class Output {
        /**
         * 基本信息
         */
        private Baseinfo baseinfo;
        /**
         * 参保信息列表
         */
        private List<Insuinfo> insuinfo;
        /**
         * 身份信息列表
         */
        private List<Idetinfo> idetinfo;
    }

    @Data
    public static class Baseinfo {
        /**
         * 人员编号
         */
        private String psn_no;
        /**
         * 人员证件类型
         */
        private String psn_cert_type;
        /**
         * 证件号码
         */
        private String certno;
        /**
         * 人员姓名
         */
        private String psn_name;
        /**
         * 性别
         */
        private String gend;
        /**
         * 民族
         */
        private String naty;
        /**
         * 出生日期
         */
        private LocalDate brdy;
        /**
         * 年龄
         */
        private BigDecimal age;
    }

    @Data
    public static class Insuinfo {
        /**
         * 余额
         */
        private BigDecimal balc;
        /**
         * 险种类型
         */
        private String insutype;
        /**
         * 人员类别
         */
        private String psn_type;
        /**
         * 人员参保状态
         */
        private String psn_insu_stas;
        /**
         * 个人参保日期
         */
        private LocalDate psn_insu_date;
        /**
         * 暂停参保日期
         */
        private LocalDate paus_insu_date;
        /**
         * 公务员标志
         */
        private String cvlserv_flag;
        /**
         * 参保地医保区划
         */
        private String insuplc_admdvs;
        /**
         * 单位名称
         */
        private String emp_name;
    }

    @Data
    public static class Idetinfo {
        /**
         * 人员身份类别
         */
        private String psn_idet_type;
        /**
         * 人员类别等级
         */
        private String psn_type_lv;
        /**
         * 备注
         */
        private String memo;
        /**
         * 开始时间
         */
        private LocalDateTime begntime;
        /**
         * 结束时间
         */
        private LocalDateTime endtime;
    }
}
