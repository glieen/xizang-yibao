package cn.glieen.yibao.xizang.api;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/9 JDK8
 * <p>
 * 参数节点
 */
public interface ApiParam {
    String paramName();

    default Object paramData() {
        return this;
    }
}
