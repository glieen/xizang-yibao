package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.Api;
import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @since 2021/7/21 JDK8
 */
@AllArgsConstructor
public class API3403 implements Api {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API3403;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 医院科室编码
         */
        private String hosp_dept_codg;
        /**
         * 医院科室名称
         */
        private String hosp_dept_name;
        /**
         * 开始时间
         */
        private LocalDate begntime;

        @Override
        public String paramName() {
            return "data";
        }
    }

}
