package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.ResultApi;
import cn.glieen.yibao.xizang.pojo.FileBean;
import lombok.AllArgsConstructor;

import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/12 JDK8
 */
@AllArgsConstructor
public class API1310 implements ResultApi<FileBean.Output> {
    // 初始版本号
    public static final String initVer = "B001_20200128192705_C";
    private final FileBean.Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API1310;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }
}
