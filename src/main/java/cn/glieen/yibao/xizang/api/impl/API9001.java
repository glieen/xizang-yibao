package cn.glieen.yibao.xizang.api.impl;


import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.ResultApi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/12 JDK8
 */
@AllArgsConstructor
public class API9001 implements ResultApi<API9001.Output> {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API9001;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 操作员编号
         */
        private String opter_no;
        /**
         * 签到 MAC 地址
         */
        private String mac;
        /**
         * 签到 IP 地址
         */
        private String ip;

        @Override
        public String paramName() {
            return "signIn";
        }
    }

    @Data
    public static class Output {
        /**
         * 签到信息
         */
        private Signinoutb signinoutb;
    }

    @Data
    public static class Signinoutb {
        /**
         * 签到时间
         */
        private LocalDateTime sign_time;
        /**
         * 签到编号
         */
        private String sign_no;
    }
}