package cn.glieen.yibao.xizang.api.impl;


import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.ResultApi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/12 JDK8
 */
@AllArgsConstructor
public class API9002 implements ResultApi<API9002.Output> {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API9002;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 操作员编号
         */
        private String opter_no;
        /**
         * 签到 MAC 地址
         */
        private String sign_no;

        @Override
        public String paramName() {
            return "signOut";
        }
    }

    @Data
    public static class Output {
        /**
         * 签退时间
         */
        private SignTime sign_time;
    }

    @Data
    public static class SignTime {
        /**
         * 签退时间
         */
        private LocalDateTime sign_time;
    }
}
