package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.Api;
import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @since 2021/7/21 JDK8
 */
@AllArgsConstructor
public class API3501 implements Api {
    private final Param param;

    @Override

    public ApiCode getApiCode() {
        return ApiCode.API3501;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 医疗目录编码
         */
        private String med_list_codg;
        /**
         * 定点医药机构目录编号
         */
        private String fixmedins_hilist_id;
        /**
         * 定点医药机构目录名称
         */
        private String fixmedins_hilist_name;
        /**
         * 处方药标志
         */
        private String rx_flag;
        /**
         * 盘存日期
         */
        private LocalDate invdate;
        /**
         * 库存数量
         */
        private String inv_cnt;
        /**
         * 生产批号
         */
        private String manu_lotnum;
        /**
         * 定点医药机构批次流水号
         */
        private String fixmedins_bchno;
        /**
         * 生产日期
         */
        private LocalDate manu_date;
        /**
         * 有效期止
         */
        private LocalDate expy_end;
        /**
         * 备注
         */
        private String memo;

        @Override
        public String paramName() {
            return "invinfo";
        }
    }

}
