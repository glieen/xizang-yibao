package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.Api;
import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/16 JDK8
 */
@AllArgsConstructor
public class API2203 implements Api {
    private final Param param;
    private final Param2 param2;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API2203;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Arrays.asList(param, param2);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 就诊
         */
        private String mdtrt_id;
        /**
         * 人员编号
         */
        private String psn_no;
        /**
         * 医疗类别
         */
        private String med_type;
        /**
         * 开始时间
         */
        private LocalDateTime begntime;
        /**
         * 主要病情描述
         */
        private String main_cond_dscr;
        /**
         * 病种编码
         */
        private String dise_codg;
        /**
         * 病种名称
         */
        private String dise_name;
        /**
         * 计划生育手术类别
         */
        private String birctrl_type;
        /**
         * 计划生育手术或生育日期
         */
        private LocalDate birctrl_matn_date;

        @Override
        public String paramName() {
            return "mdtrtinfo";
        }
    }

    @Data
    public static class Param2 implements ApiParam {
        /**
         * 诊断信息
         */
        private List<Diseinfo> data;

        @Override
        public String paramName() {
            return "diseinfo";
        }

        @Override
        public Object paramData() {
            return this.data;
        }
    }

    @Data
    public static class Diseinfo {
        /**
         * 诊断类别
         */
        private String diag_type;
        /**
         * 诊断排序号
         */
        private Integer diag_srt_no;
        /**
         * 诊断代码
         */
        private String diag_code;
        /**
         * 诊断名称
         */
        private String diag_name;
        /**
         * 诊断科室
         */
        private String diag_dept;
        /**
         * 诊断医生编码
         */
        private String dise_dor_no;
        /**
         * 诊断医生姓名
         */
        private String dise_dor_name;
        /**
         * 诊断时间
         */
        private LocalDateTime diag_time;
        /**
         * 有效标志
         */
        private String vali_flag;
    }
}
