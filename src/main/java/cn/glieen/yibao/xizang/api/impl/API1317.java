package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.PageApi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/14 JDK8
 */
@AllArgsConstructor
public class API1317 implements PageApi<API1317.Record> {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API1317;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 查询时间点
         */
        private LocalDate query_date;
        /**
         * 定点医药机构编号
         */
        private String fixmedins_code;
        /**
         * 定点医药机构目录编号
         */
        private String medins_list_codg;
        /**
         * 定点医药机构目录名称
         */
        private String medins_list_name;
        /**
         * 参保机构医保区划
         */
        private String insu_admdvs;
        /**
         * 目录类别
         */
        private String list_type;
        /**
         * 医疗目录编码
         */
        private String med_list_codg;
        /**
         * 开始日期
         */
        private LocalDate begndate;
        /**
         * 有效标志
         */
        private String vali_flag;
        /**
         * 更新时间
         */
        private LocalDate updt_time;
        /**
         * 当前页数
         */
        private Integer page_num;
        /**
         * 本页数据量
         */
        private Integer page_size;

        @Override
        public String paramName() {
            return "data";
        }
    }

    @Data
    public static class Record {
        /**
         * 定点医药机构编号
         */
        private String fixmedins_code;
        /**
         * 定点医药机构目录编号
         */
        private String medins_list_codg;
        /**
         * 定点医药机构目录名称
         */
        private String medins_list_name;
        /**
         * 参保机构医保区划
         */
        private String insu_admdvs;
        /**
         * 目录类别
         */
        private String list_type;
        /**
         * 医疗目录编码
         */
        private String med_list_codg;
        /**
         * 开始日期
         */
        private LocalDateTime begndate;
        /**
         * 结束日期
         */
        private LocalDateTime enddate;
        /**
         * 批准文号
         */
        private String aprvno;
        /**
         * 剂型
         */
        private String dosform;
        /**
         * 除外内容
         */
        private String exct_cont;
        /**
         * 项目内涵
         */
        private String item_cont;
        /**
         * 计价单位
         */
        private String prcunt;
        /**
         * 规格
         */
        private String spec;
        /**
         * 包装规格
         */
        private String pacspec;
        /**
         * 备注
         */
        private String memo;
        /**
         * 有效标志
         */
        private String vali_flag;
        /**
         * 唯一记录号
         */
        private String rid;
        /**
         * 更新时间
         */
        private LocalDateTime updt_time;
        /**
         * 创建人
         */
        private String crter_id;
        /**
         * 创建人姓名
         */
        private String crter_name;
        /**
         * 创建时间
         */
        private LocalDateTime crte_time;
        /**
         * 创建机构
         */
        private String crte_optins_no;
        /**
         * 经办人
         */
        private String opter_id;
        /**
         * 经办人姓名
         */
        private String opter_name;
        /**
         * 经办时间
         */
        private LocalDateTime opt_time;
        /**
         * 经办机构
         */
        private String optins_no;
        /**
         * 统筹区
         */
        private String poolarea_no;
    }
}
