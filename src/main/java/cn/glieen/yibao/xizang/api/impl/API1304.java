package cn.glieen.yibao.xizang.api.impl;


import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.PageApi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/12 JDK8
 */
@AllArgsConstructor
public class API1304 implements PageApi<API1304.Record> {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API1304;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 医疗目录编码
         */
        private String med_list_codg;
        /**
         * 通用名编号
         */
        private String genname_codg;
        /**
         * 药品通用名
         */
        private String drug_genname;
        /**
         * 药品商品名
         */
        private String drug_prodname;
        /**
         * 注册名称
         */
        private String reg_name;
        /**
         * 中草药名称
         */
        private String tcmherb_name;
        /**
         * 药材名称
         */
        private String mlms_name;
        /**
         * 有效标志
         */
        private String vali_flag;
        /**
         * 唯一记录号
         */
        private String rid;
        /**
         * 版本号
         */
        private String ver;
        /**
         * 版本名称
         */
        private String ver_name;
        /**
         * 经办开始时间
         */
        private LocalDate opt_begn_time;
        /**
         * 经办结束时间
         */
        private LocalDate opt_end_time;
        /**
         * 更新时间
         */
        private LocalDate updt_time;
        /**
         * 当前页数
         */
        private Integer page_num;
        /**
         * 本页数据量
         */
        private Integer page_size;

        @Override
        public String paramName() {
            return "data";
        }
    }

    @Data
    public static class Record {
        /**
         * 医疗目录编码
         */
        private String med_list_codg;
        /**
         * 药品商品名
         */
        private String drug_prodname;
        /**
         * 通用名编号
         */
        private String genname_codg;
        /**
         * 药品通用名
         */
        private String drug_genname;
        /**
         * 民族药种类
         */
        private String ethdrug_type;
        /**
         * 化学名称
         */
        private String chemname;
        /**
         * 别名
         */
        private String alis;
        /**
         * 英文名称
         */
        private String eng_name;
        /**
         * 剂型
         */
        private String dosform;
        /**
         * 每次用量
         */
        private String each_dos;
        /**
         * 使用频次
         */
        private String used_frqu;
        /**
         * 国家药品编号
         */
        private String nat_drug_no;
        /**
         * 用法
         */
        private String used_mtd;
        /**
         * 成分
         */
        private String ing;
        /**
         * 性状
         */
        private String chrt;
        /**
         * 不良反应
         */
        private String defs;
        /**
         * 禁忌
         */
        private String tabo;
        /**
         * 注意事项
         */
        private String mnan;
        /**
         * 贮藏
         */
        private String stog;
        /**
         * 药品规格
         */
        private String drug_spec;
        /**
         * 计价单位类型
         */
        private String prcunt_type;
        /**
         * 非处方药标志
         */
        private String otc_flag;
        /**
         * 包装材质
         */
        private String pacmatl;
        /**
         * 包装规格
         */
        private String pacspec;
        /**
         * 最小使用单位
         */
        private String min_useunt;
        /**
         * 最小销售单位
         */
        private String min_salunt;
        /**
         * 说明书
         */
        private String manl;
        /**
         * 给药途径
         */
        private String rute;
        /**
         * 开始日期
         */
        private String begndate;
        /**
         * 结束日期
         */
        private String enddate;
        /**
         * 药理分类
         */
        private String pham_type;
        /**
         * 备注
         */
        private String memo;
        /**
         * 包装数量
         */
        private String pac_cnt;
        /**
         * 最小计量单位
         */
        private String min_unt;
        /**
         * 最小包装数量
         */
        private BigDecimal min_pac_cnt;
        /**
         * 最小包装单位
         */
        private String min_pacunt;
        /**
         * 最小制剂单位
         */
        private String min_prepunt;
        /**
         * 药品有效期
         */
        private String drug_expy;
        /**
         * 功能主治
         */
        private String efcc_atd;
        /**
         * 最小计价单位
         */
        private String min_prcunt;
        /**
         * 五笔助记码
         */
        private String wubi;
        /**
         * 拼音助记码
         */
        private String pinyin;
        /**
         * 有效标志
         */
        private String vali_flag;
        /**
         * 唯一记录号
         */
        private String rid;
        /**
         * 数据创建时间
         */
        private LocalDate crte_time;
        /**
         * 数据更新时间
         */
        private LocalDate updt_time;
        /**
         * 创建人
         */
        private String crter_id;
        /**
         * 创建人姓名
         */
        private String crter_name;
        /**
         * 创建经办机构
         */
        private String crte_optins_no;
        /**
         * 经办人
         */
        private String opter_id;
        /**
         * 经办人姓名
         */
        private String opter_name;
        /**
         * 经办时间
         */
        private LocalDate opt_time;
        /**
         * 经办机构
         */
        private String optins_no;
        /**
         * 版本号
         */
        private String ver;
    }
}
