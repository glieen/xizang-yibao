package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.ResultApi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/19 JDK8
 */
@AllArgsConstructor
public class API2505 implements ResultApi<API2505.Output> {
    @Override
    public ApiCode getApiCode() {
        return ApiCode.API2505;
    }

    @Override
    public List<ApiParam> getParamList() {
        return null;
    }

    @Data
    public static class Param implements ApiParam {

        /**
         * 人员编号
         */
        private String psn_no;
        /**
         * 联系电话
         */
        private String tel;
        /**
         * 联系地址
         */
        private String addr;
        /**
         * 业务申请类型
         */
        private String biz_appy_type;
        /**
         * 开始日期
         */
        private LocalDate begndate;
        /**
         * 结束日期
         */
        private LocalDate enddate;
        /**
         * 代办人姓名
         */
        private String agnter_name;
        /**
         * 代办人证件类型
         */
        private String agnter_cert_type;
        /**
         * 代办人证件号码
         */
        private String agnter_certno;
        /**
         * 代办人联系方式
         */
        private String agnter_tel;
        /**
         * 代办人联系地址
         */
        private String agnter_addr;
        /**
         * 代办人关系
         */
        private String agnter_rlts;
        /**
         * 定点排序号
         */
        private String fix_srt_no;
        /**
         * 定点医药机构编号
         */
        private String fixmedins_code;
        /**
         * 定点医药机构名称
         */
        private String fixmedins_name;
        /**
         * 备注
         */
        private String memo;

        @Override
        public String paramName() {
            return "data";
        }
    }

    @Data
    public static class Output {
        /**
         * 输出
         */
        private Result result;
    }

    @Data
    public static class Result {
        /**
         * 待遇申报明细流水号
         */
        private String trt_dcla_detl_sn;
    }
}
