package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.PageApi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/14 JDK8
 */
@AllArgsConstructor
public class API1312 implements PageApi<API1312.Record> {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API1312;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 查询时间点
         */
        private LocalDate query_date;
        /**
         * 医保目录编码
         */
        private String hilist_code;
        /**
         * 参保机构医保区划
         */
        private String insu_admdvs;
        /**
         * 开始日期
         */
        private LocalDate begndate;
        /**
         * 医保目录名称
         */
        private String hilist_name;
        /**
         * 五笔助记码
         */
        private String wubi;
        /**
         * 拼音助记码
         */
        private String pinyin;
        /**
         * 医疗收费项目类别
         */
        private String med_chrgitm_type;
        /**
         * 收费项目等级
         */
        private String chrgitm_lv;
        /**
         * 限制使用标志
         */
        private String lmt_used_flag;
        /**
         * 目录类别
         */
        private String list_type;
        /**
         * 医疗使用标志
         */
        private String med_use_flag;
        /**
         * 生育使用标志
         */
        private String matn_used_flag;
        /**
         * 医保目录使用类别
         */
        private String hilist_use_type;
        /**
         * 限复方使用类型
         */
        private String lmt_cpnd_type;
        /**
         * 有效标志
         */
        private String vali_flag;
        /**
         * 更新时间
         */
        private LocalDate updt_time;
        /**
         * 当前页数
         */
        private Integer page_num;
        /**
         * 本页数据量
         */
        private Integer page_size;

        @Override
        public String paramName() {
            return "data";
        }
    }

    @Data
    public static class Record {
        /**
         * 医保目录编码
         */
        private String hilist_code;
        /**
         * 医保目录名称
         */
        private String hilist_name;
        /**
         * 参保机构医保区划
         */
        private String insu_admdvs;
        /**
         * 开始日期
         */
        private String begndate;
        /**
         * 结束日期
         */
        private String enddate;
        /**
         * 医疗收费项目类别
         */
        private String med_chrgitm_type;
        /**
         * 收费项目等级
         */
        private String chrgitm_lv;
        /**
         * 限制使用标志
         */
        private String lmt_used_flag;
        /**
         * 目录类别
         */
        private String list_type;
        /**
         * 医疗使用标志
         */
        private String med_use_flag;
        /**
         * 生育使用标志
         */
        private String matn_used_flag;
        /**
         * 医保目录使用类别
         */
        private String hilist_use_type;
        /**
         * 限复方使用类型
         */
        private String lmt_cpnd_type;
        /**
         * 五笔助记码
         */
        private String wubi;
        /**
         * 拼音助记码
         */
        private String pinyin;
        /**
         * 备注
         */
        private String memo;
        /**
         * 有效标志
         */
        private String vali_flag;
        /**
         * 唯一记录号
         */
        private String rid;
        /**
         * 更新时间
         */
        private String updt_time;
        /**
         * 创建人
         */
        private String crter_id;
        /**
         * 创建人姓名
         */
        private String crter_name;
        /**
         * 创建时间
         */
        private String crte_time;
        /**
         * 创建机构
         */
        private String crte_optins_no;
        /**
         * 经办人
         */
        private String opter_id;
        /**
         * 经办人姓名
         */
        private String opter_name;
        /**
         * 经办时间
         */
        private String opt_time;
        /**
         * 经办机构
         */
        private String optins_no;
        /**
         * 统筹区
         */
        private String poolarea_no;
    }
}
