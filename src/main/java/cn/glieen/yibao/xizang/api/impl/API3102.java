package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.ResultApi;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/19 JDK8
 * <p>
 * TODO 接口文档描述不明确
 */
@AllArgsConstructor
public class API3102 implements ResultApi<Object> {
    @Override
    public ApiCode getApiCode() {
        return ApiCode.API3102;
    }

    @Override
    public List<ApiParam> getParamList() {
        return null;
    }
}
