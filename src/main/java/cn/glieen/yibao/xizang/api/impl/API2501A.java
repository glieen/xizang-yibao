package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.ResultApi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/19 JDK8
 */
@AllArgsConstructor
public class API2501A implements ResultApi<API2501A.Output> {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API2501A;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {

        /**
         * 人员编号
         */
        private String psn_no;
        /**
         * 险种类型
         */
        private String insutype;
        /**
         * 联系电话
         */
        private String tel;
        /**
         * 联系地址
         */
        private String addr;
        /**
         * 参保机构医保区划
         */
        private String insu_optins;
        /**
         * 诊断代码
         */
        private String diag_code;
        /**
         * 诊断名称
         */
        private String diag_name;
        /**
         * 疾病病情描述
         */
        private String dise_cond_dscr;
        /**
         * 转往定点医药机构编号
         */
        private String reflin_medins_no;
        /**
         * 转往医院名称
         */
        private String reflin_medins_name;
        /**
         * 就医地行政区划
         */
        private String mdtrtarea_admdvs;
        /**
         * 医院同意转院标志
         */
        private String hosp_agre_refl_flag;
        /**
         * 转院类型
         */
        private String refl_type;
        /**
         * 转院日期
         */
        private LocalDate refl_date;
        /**
         * 转院原因
         */
        private String refl_rea;
        /**
         * 转院意见
         */
        private String refl_opnn;
        /**
         * 开始日期
         */
        private LocalDate begndate;
        /**
         * 结束日期
         */
        private LocalDate enddate;
        /**
         * 转院前就诊id
         */
        private String refl_old_mdtrt_id;

        @Override
        public String paramName() {
            return "refmedin";
        }
    }

    @Data
    public static class Output {
        /**
         * 输出
         */
        private Result result;
    }

    @Data
    public static class Result {
        /**
         * 待遇申报明细流水号
         */
        private String trt_dcla_detl_sn;
    }
}
