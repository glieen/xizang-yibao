package cn.glieen.yibao.xizang.api;

import cn.glieen.yibao.xizang.pojo.PageBean;
import cn.glieen.yibao.xizang.util.CommonUtil;
import cn.glieen.yibao.xizang.util.JsonUtil;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/14 JDK8
 */
public interface PageApi<T> extends Api {

    @SuppressWarnings("unchecked")
    default PageBean<T> page(Map<String, Object> output) {
        PageBean<T> page = JsonUtil.toObject(output, PageBean.class);
        Class<T> clazz = CommonUtil.getGenericType(this.getClass(), PageApi.class);
        if (page.getData() != null) {
            List<T> list = page.getData().stream().map(t -> JsonUtil.toObject((Map<?, ?>) t, clazz)).collect(Collectors.toList());
            page.setData(list);
        }
        return page;
    }
}
