package cn.glieen.yibao.xizang.api.impl;


import cn.glieen.yibao.xizang.api.Api;
import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/12 JDK8
 */
@AllArgsConstructor
public class API9102 implements Api {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API9102;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 文件查询号
         */
        private String file_qury_no;
        /**
         * 文件名
         */
        private String filename;
        /**
         * 医药机构编号
         */
        private String fixmedins_code;

        @Override
        public String paramName() {
            return "fsDownloadIn";
        }
    }
}