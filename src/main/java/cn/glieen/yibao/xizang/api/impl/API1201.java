package cn.glieen.yibao.xizang.api.impl;

import cn.glieen.yibao.xizang.api.ApiCode;
import cn.glieen.yibao.xizang.api.ApiParam;
import cn.glieen.yibao.xizang.api.ResultApi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collections;
import java.util.List;

/**
 * @author Glieen glieen1995@gmail.com
 * @version 1.0
 * @since 2021/7/13 JDK8
 */
@AllArgsConstructor
public class API1201 implements ResultApi<API1201.Output> {
    private final Param param;

    @Override
    public ApiCode getApiCode() {
        return ApiCode.API1201;
    }

    @Override
    public List<ApiParam> getParamList() {
        return Collections.singletonList(param);
    }

    @Data
    public static class Param implements ApiParam {
        /**
         * 定点医疗服务机构类型
         */
        private String fixmedins_type;
        /**
         * 定点医药机构名称
         */
        private String fixmedins_name;
        /**
         * 定点医药机构编号
         */
        private String fixmedins_code;

        @Override
        public String paramName() {
            return "medinsinfo";
        }
    }

    @Data
    public static class Output {
        private List<Medinsinfo> medinsinfo;
    }

    @Data
    public static class Medinsinfo {
        /**
         * 定点医药机构编号
         */
        private String fixmedins_code;
        /**
         * 定点医药机构名称
         */
        private String fixmedins_name;
        /**
         * 统一社会信用代码
         */
        private String uscc;
        /**
         * 定点医疗服务机构类型
         */
        private String fixmedins_type;
        /**
         * 医院等级
         */
        private String hosp_lv;
    }
}
